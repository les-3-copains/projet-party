package fr.triocode.projectparty.data.enums;

public enum TypeEvening {
    CLASSIC,
    BOARD_GAME,
    VIDEO_GAME;
}
