package fr.triocode.projectparty.dto.response;

import fr.triocode.projectparty.data.enums.TypeStock;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class StockDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private String name;
    private TypeStock type;
    private UserDTO bringer;
}
