package fr.triocode.projectparty.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.joda.time.LocalDateTime;

@Data
public class RequestMessageDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private String message;
    private int userId;
    private int eveningId;
}
