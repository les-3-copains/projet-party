package fr.triocode.projectparty.repository;

import fr.triocode.projectparty.data.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockRepository extends JpaRepository<Stock, Integer> {
    List<Stock> findAllByEvening_id(int eveningId);
}
