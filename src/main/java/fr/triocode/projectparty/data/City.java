package fr.triocode.projectparty.data;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@Table(name = "city", indexes = {
    @Index(name = "idx_city_name", columnList = "name"),
    @Index(name = "idx_city_zip_code", columnList = "zip_code")
})
@AllArgsConstructor
@NoArgsConstructor
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 50)
    private String name;

    @Column(name = "zip_code", nullable = false)
    private int zipCode;
}
