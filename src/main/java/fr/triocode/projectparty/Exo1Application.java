package fr.triocode.projectparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableCaching
@EnableJpaRepositories
public class Exo1Application {

	public static void main(String[] args) {
		SpringApplication.run(Exo1Application.class, args);
	}

}
