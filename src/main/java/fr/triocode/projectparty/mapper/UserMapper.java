package fr.triocode.projectparty.mapper;

import fr.triocode.projectparty.data.User;
import fr.triocode.projectparty.dto.request.RegisterUserDTO;
import fr.triocode.projectparty.dto.response.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    User toEntity(RegisterUserDTO requestUserDTO);
    UserDTO toDto(User user);
}
