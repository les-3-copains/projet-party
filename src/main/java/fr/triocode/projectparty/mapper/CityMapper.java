package fr.triocode.projectparty.mapper;

import fr.triocode.projectparty.data.City;
import fr.triocode.projectparty.dto.CityDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CityMapper {
    City toEntity(CityDTO CityDTO);
    CityDTO toDTO(City City);
}
