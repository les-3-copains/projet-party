package fr.triocode.projectparty.dto.request;

import fr.triocode.projectparty.data.enums.TypeStock;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class RequestStockDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private String name;
    private TypeStock type;
    private int bringerId;
    private int eveningId;
}
