package fr.triocode.projectparty.mapper;

import fr.triocode.projectparty.data.Stock;
import fr.triocode.projectparty.dto.request.RequestStockDTO;
import fr.triocode.projectparty.dto.response.StockDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(uses = UserMapper.class, componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StockMapper {
    Stock toEntity(RequestStockDTO requestStockDTO);
    StockDTO toDTO(Stock stock);
}
