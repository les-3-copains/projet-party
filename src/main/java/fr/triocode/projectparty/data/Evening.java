package fr.triocode.projectparty.data;

import fr.triocode.projectparty.data.enums.TypeEvening;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "evening", indexes = {
    @Index(name = "idx_evening_start_date", columnList = "start_date"),
    @Index(name = "idx_evening_end_date", columnList = "end_date"),
    @Index(name = "idx_evening_type", columnList = "type")
})

@AllArgsConstructor
@NoArgsConstructor
public class Evening {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "evening_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "organizer_id")
    private User organizer;

    @Column(nullable = false, length = 50)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "adresse_id")
    private Address address;

    @Column(name = "max_guests")
    private int maxGuests;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "evening_guest", joinColumns = @JoinColumn(name = "evening_id"), inverseJoinColumns =
    @JoinColumn(name = "user_id"))
    private List<User> guests;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "evening_participant", joinColumns = @JoinColumn(name = "evening_id"), inverseJoinColumns =
    @JoinColumn(name = "user_id"))
    private List<User> participants;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "evening_blacklist", joinColumns = @JoinColumn(name = "evening_id"), inverseJoinColumns =
    @JoinColumn(name = "user_id"))
    private List<User> blacklisted;

    @OneToMany(mappedBy = "evening", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Message> messages;

    @Column(length = 500)
    private String description;

    @Column(columnDefinition = "float default 0")
    private double price;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "create_date")
    private Date createDate;

    @Enumerated(EnumType.STRING)
    private TypeEvening type;

    @Column(name = "is_bring_food")
    private boolean isBringFood;

    @Column(name = "is_bring_video_game")
    private boolean isBringVideoGame;

    @Column(name = "is_bring_board_game")
    private boolean isBringBoardGame;

    @OneToMany(mappedBy = "evening", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Stock> stocks;
}
