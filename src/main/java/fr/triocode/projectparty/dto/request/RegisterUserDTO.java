package fr.triocode.projectparty.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class RegisterUserDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private String city;
    private Date birthday;
    private List<String> interests;
    private String name;
    private String password;
}
