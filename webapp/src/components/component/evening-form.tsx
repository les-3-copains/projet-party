'use client'
import { Button } from "@/components/ui/button"
import { Label } from "@/components/ui/label"
import { Input } from "@/components/ui/input"
import { Select, SelectTrigger, SelectValue, SelectContent, SelectItem } from "@/components/ui/select"
import { Textarea } from "@/components/ui/textarea"
import { Avatar, AvatarImage, AvatarFallback } from "@/components/ui/avatar"
import { Popover, PopoverTrigger, PopoverContent } from "@/components/ui/popover"
import { Calendar } from "@/components/ui/calendar"
import { Checkbox } from "@/components/ui/checkbox"
import {XIcon, CalendarDaysIcon} from "lucide-react";
import {useState} from "react";


interface Organizer {
  id: number;
  name: string;
  email: string;
}

interface Guest {
  id: number;
  name: string;
  email: string;
}

interface Stock {
  item: string;
  quantity: number;
}

interface Address {
  street: string;
  city: string;
  postalCode: string;
  country: string;
}

interface Evening {
  id: number;
  organizer: Organizer;
  name: string;
  address: Address;
  description: string;
  maxGuest: number;
  guests: Guest[];
  price: number;
  startDate: string;
  endDate: string;
  createdDate: string;
  type: string;
  isBringFood: boolean;
  isBringVideoGame: boolean;
  isBringBoardGame: boolean;
  stocks: Stock[];
}


export function EveningForm() {
  const [events, setEvents] = useState<Evening>();

  return (
      <div className="fixed inset-0 z-50 flex items-center justify-center bg-black/50">
        <div className="bg-background p-6 rounded-lg shadow-lg w-full max-w-[800px]">
          <div className="flex items-center justify-between mb-6">
            <div>
              <h2 className="text-2xl font-bold">{events?.name}</h2>
              <p className="text-muted-foreground">Organisée par {events?.organizer.name}</p>
            </div>
            <Button variant="ghost" size="icon" className="text-muted-foreground hover:bg-muted/50">
              <XIcon className="w-5 h-5"/>
            </Button>
          </div>
          <div className="grid gap-6 py-6">
            <div className="grid grid-cols-2 gap-4">
              <div className="space-y-2">
                <Label htmlFor="name">Nom</Label>
                <Input id="name" value={events?.name} disabled/>
              </div>
              <div className="space-y-2">
                <Label htmlFor="organizer">Organisateur</Label>
                <Input id="organizer" value={events?.organizer.name} disabled/>
              </div>
            </div>
            <div className="grid grid-cols-2 gap-4">
              <div className="space-y-2">
                <Label htmlFor="name">Nom</Label>
                <Input id="name" placeholder="Entrez votre nom"/>
              </div>
              <div className="space-y-2">
                <Label htmlFor="email">Email</Label>
                <Input id="email" type="email" placeholder="Entrez votre email"/>
              </div>
            </div>
            <div className="grid grid-cols-3 gap-4">
              <div className="space-y-2">
                <Label htmlFor="address">Adresse</Label>
                <Input id="address" placeholder="Entrez votre adresse"/>
              </div>
              <div className="space-y-2">
                <Label htmlFor="city">Ville</Label>
                <Input id="city" placeholder="Entrez votre ville"/>
              </div>
              <div className="space-y-2">
                <Label htmlFor="zipcode">Code postal</Label>
                <Input id="zipcode" placeholder="Entrez votre code postal"/>
              </div>
            </div>
            <div className="grid grid-cols-2 gap-4">
              <div className="space-y-2">
                <Label htmlFor="country">Pays</Label>
                <Select>
                  <SelectTrigger>
                    <SelectValue placeholder="Sélectionnez un pays"/>
                  </SelectTrigger>
                  <SelectContent>
                    <SelectItem value="usa">États-Unis</SelectItem>
                    <SelectItem value="canada">Canada</SelectItem>
                    <SelectItem value="france">France</SelectItem>
                    <SelectItem value="germany">Allemagne</SelectItem>
                  </SelectContent>
                </Select>
              </div>
              <div className="space-y-2">
                <Label htmlFor="description">Description</Label>
                <Textarea id="description" placeholder="Entrez une description"/>
              </div>
            </div>
            <div className="grid grid-cols-2 gap-4">
              <div className="space-y-2">
                <Label htmlFor="max-guests">Nombre max. d'invités</Label>
                <Input id="max-guests" type="number" placeholder="Entrez le nombre max. d'invités"/>
              </div>
              <div className="space-y-2">
                <Label>Liste des invités</Label>
                <div className="grid gap-2">
                  <div className="flex items-center justify-between gap-4">
                    <div className="flex items-center gap-2">
                      <Avatar>
                        <AvatarImage src="/placeholder-user.jpg"/>
                        <AvatarFallback>JD</AvatarFallback>
                      </Avatar>
                      <div>
                        <p className="font-medium">John Doe</p>
                        <p className="text-sm text-muted-foreground">john@example.com</p>
                      </div>
                    </div>
                    <p className="text-sm font-medium">$25</p>
                  </div>
                  <div className="flex items-center justify-between gap-4">
                    <div className="flex items-center gap-2">
                      <Avatar>
                        <AvatarImage src="/placeholder-user.jpg"/>
                        <AvatarFallback>JA</AvatarFallback>
                      </Avatar>
                      <div>
                        <p className="font-medium">Jane Appleseed</p>
                        <p className="text-sm text-muted-foreground">jane@example.com</p>
                      </div>
                    </div>
                    <p className="text-sm font-medium">$25</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="grid grid-cols-3 gap-4">
              <div className="space-y-2">
                <Label htmlFor="start-date">Date de début</Label>
                <Popover>
                  <PopoverTrigger asChild>
                    <Button variant="outline" className="w-full text-left">
                      <span>2023-06-15</span>
                      <CalendarDaysIcon className="ml-auto h-4 w-4 opacity-50"/>
                    </Button>
                  </PopoverTrigger>
                  <PopoverContent className="p-0">
                    <Calendar/>
                  </PopoverContent>
                </Popover>
              </div>
              <div className="space-y-2">
                <Label htmlFor="end-date">Date de fin</Label>
                <Popover>
                  <PopoverTrigger asChild>
                    <Button variant="outline" className="w-full text-left">
                      <span>2023-06-16</span>
                      <CalendarDaysIcon className="ml-auto h-4 w-4 opacity-50"/>
                    </Button>
                  </PopoverTrigger>
                  <PopoverContent className="p-0">
                    <Calendar/>
                  </PopoverContent>
                </Popover>
              </div>
              <div className="space-y-2">
                <Label htmlFor="created-at">Date de création</Label>
                <Input id="created-at" value="2023-06-01" disabled/>
              </div>
            </div>
            <div className="grid grid-cols-2 gap-4">
              <div className="space-y-2">
                <Label htmlFor="event-type">Type de soirée</Label>
                <Select>
                  <SelectTrigger>
                    <SelectValue placeholder="Sélectionnez un type de soirée"/>
                  </SelectTrigger>
                  <SelectContent>
                    <SelectItem value="dinner">Repas</SelectItem>
                    <SelectItem value="video-games">Jeux vidéo</SelectItem>
                    <SelectItem value="board-games">Jeux de société</SelectItem>
                  </SelectContent>
                </Select>
              </div>
              <div className="space-y-2">
                <Label>Apporter</Label>
                <div className="grid grid-cols-3 gap-2">
                  <div className="flex items-center gap-2">
                    <Checkbox id="bring-food"/>
                    <Label htmlFor="bring-food">Nourriture</Label>
                  </div>
                  <div className="flex items-center gap-2">
                    <Checkbox id="bring-video-games"/>
                    <Label htmlFor="bring-video-games">Jeux vidéo</Label>
                  </div>
                  <div className="flex items-center gap-2">
                    <Checkbox id="bring-board-games"/>
                    <Label htmlFor="bring-board-games">Jeux de société</Label>
                  </div>
                </div>
              </div>
            </div>
            <div className="grid grid-cols-2 gap-4">
              <div className="space-y-2">
                <Label htmlFor="stocks">Stocks</Label>
                <Textarea id="stocks" placeholder="Entrez les articles et quantités"/>
              </div>
            </div>
          </div>
          <div className="flex justify-end gap-4">
            <Button variant="outline">Fermer</Button>
            <Button type="submit">Enregistrer</Button>
          </div>
        </div>
      </div>
  )
}
