package fr.triocode.projectparty.data;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "utilisateur", indexes = {
        @Index(name = "idx_user_name", columnList = "name"),
        @Index(name = "idx_user_city", columnList = "city")
})
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    private String city;

    private Date birthday;

    @ElementCollection
    @CollectionTable(name = "user_interest", joinColumns = @JoinColumn(name = "id_user"))
    @Column(name = "interest")
    private List<String> interests;

    private String name;

    private String password;

    @OneToMany
    private List<Evening> organizedEvenings;

    @ManyToMany
    @JoinTable(name = "evening_guest", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns =
    @JoinColumn(name = "evening_id"))
    private List<Evening> guestedEvenings;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "evening_paticipant", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns =
    @JoinColumn(name = "evening_id"))
    private List<Evening> participatedEvenings;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "evening_blacklist", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns =
    @JoinColumn(name = "evening_id"))
    private List<Evening> blacklistedEvenings;

    @ElementCollection
    @CollectionTable(name = "user_rates_guest", joinColumns = @JoinColumn(name = "id_user"))
    @Column(name = "rate_guest")
    private List<Integer> ratesGuest;

    @ElementCollection
    @CollectionTable(name = "user_rates_organizer", joinColumns = @JoinColumn(name = "id_user"))
    @Column(name = "rate_organizer")
    private List<Integer> ratesOrganizer;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Message> messages;
}

