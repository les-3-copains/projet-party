package fr.triocode.projectparty.mapper;

import fr.triocode.projectparty.data.Evening;
import fr.triocode.projectparty.dto.request.RequestEveningDTO;
import fr.triocode.projectparty.dto.response.EveningDTO;
import fr.triocode.projectparty.dto.response.LimitedEveningDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(uses = {UserMapper.class, AddressMapper.class, StockMapper.class, MessageMapper.class},
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring")
public interface EveningMapper {

    Evening toRequestEntity(RequestEveningDTO requestEveningDTO);
    LimitedEveningDTO toLimitedDto(Evening evening);
    EveningDTO toDto(Evening evening);
}
