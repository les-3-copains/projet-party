package fr.triocode.projectparty.data;

import fr.triocode.projectparty.data.enums.TypeStock;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "stock", indexes = {
    @Index(name = "idx_stock_bringer", columnList = "id_user"),
    @Index(name = "idx_stock_evening", columnList = "id_evening")
})
@AllArgsConstructor
@NoArgsConstructor
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @Enumerated(EnumType.STRING)
    private TypeStock type;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User bringer;

    @ManyToOne
    @JoinColumn(name = "id_evening")
    private Evening evening;
}
