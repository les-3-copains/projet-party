package fr.triocode.projectparty.repository;

import fr.triocode.projectparty.data.Address;
import fr.triocode.projectparty.data.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
    boolean existsByNumberAndStreetAndCity(int number, String street, City city);
    Address findByNumberAndStreetAndCity(int number, String street, City city);
}
