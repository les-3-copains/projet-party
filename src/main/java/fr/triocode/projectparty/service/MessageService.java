package fr.triocode.projectparty.service;

import fr.triocode.projectparty.data.Message;
import fr.triocode.projectparty.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;

    @Cacheable(value = "messages", key = "#id")
    public Optional<Message> getMessageById(int id) {
        return messageRepository.findById(id);
    }

    @CachePut(value = "messages", key = "#message.id")
    public Message saveMessage(Message message) {
        return messageRepository.save(message);
    }

    @CacheEvict(value = "messages", key = "#id")
    public boolean removeMessage(int id) {
        if (messageRepository.existsById(id)) {
            messageRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
