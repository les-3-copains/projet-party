"use client"

import {Card, CardContent, CardDescription, CardHeader, CardTitle, CardFooter} from "@/components/ui/card";
import {CalendarIcon, MapPinIcon} from "lucide-react";
import { Button } from "@/components/ui/button";
import {useState} from "react";
import {useRouter} from "next/navigation";


interface Organizer {
    id: number;
    name: string;
    email: string;
}

interface Guest {
    id: number;
    name: string;
    email: string;
}

interface Stock {
    item: string;
    quantity: number;
}

interface Address {
    street: string;
    city: string;
    postalCode: string;
    country: string;
}

interface Evening {
    id: number;
    organizer: Organizer;
    name: string;
    address: Address;
    description: string;
    maxGuest: number;
    guests: Guest[];
    price: number;
    startDate: string;
    endDate: string;
    createdDate: string;
    type: string;
    isBringFood: boolean;
    isBringVideoGame: boolean;
    isBringBoardGame: boolean;
    stocks: Stock[];
}

interface CardPartyProps {
    evening: Evening;
}

export function formatDateTime(dateString:string): string {
    const date = new Date(dateString);
    const optionsDate: Intl.DateTimeFormatOptions = { month: 'short', day: 'numeric', year:'numeric' };
    const formattedDate = date.toLocaleDateString('fr-FR', optionsDate);
    const hours = date.getHours();
    const minutes = date.getMinutes().toString().padStart(2, '0');
    return `${formattedDate} à ${hours}h${minutes}`;
}

export function CardParty(
    {
        evening,
        evening: {
            id,
            name: title,
            address: {city: location},
            description,
            startDate: dateStart,
            endDate: dateEnd,
            type,
        }
    }: CardPartyProps
) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const router = useRouter();
    const handleChoose = () => {
        localStorage.setItem('selectedEvening', JSON.stringify(evening));
        router.push('/evening');
    };

    const handleOpenModal = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
    };

    return (
        <Card className="bg-white dark:bg-gray-950 rounded-lg shadow-md flex flex-col justify-between">
            <CardHeader>
                <CardTitle>{title}</CardTitle>
                <CardDescription>
                    <div className="flex items-center gap-2">
                        <CalendarIcon className="w-4 h-4" />
                        <span className="text-gray-600 dark:text-gray-400">du : {formatDateTime(dateStart)}</span>
                    </div>
                    <div className="flex items-center gap-2">
                        <CalendarIcon className="w-4 h-4" />
                        <span className="text-gray-600 dark:text-gray-400">au : {formatDateTime(dateEnd)}</span>
                    </div>
                    <div className="flex items-center gap-2">
                        <MapPinIcon className="w-4 h-4" />
                        <div className="text-gray-600 dark:text-gray-400">{location}</div>
                    </div>
                </CardDescription>
            </CardHeader>
            <CardContent className="p-4 flex-grow">
                <div className="text-gray-600 dark:text-gray-400 mb-2">{type}</div>
                <div className="text-gray-600 dark:text-gray-400">{description}</div>
            </CardContent>
            <CardFooter className="flex justify-end">
                <Button onClick={handleChoose}>Choisir</Button>
            </CardFooter>
        </Card>
    )
}
