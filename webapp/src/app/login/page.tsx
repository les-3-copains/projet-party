'use client'

import React, { useState } from "react";
import { useRouter } from "next/navigation";
import { LoginForm } from "@/components/component/login-form";

export default function Login() {
    const router = useRouter();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        setLoading(true);

        try {
            if (username === 'nicolas' && password === '1234') {
                localStorage.setItem('account', 'authenticated');
                router.push('/');
            } else {
                setError('Invalid credentials. Please try again.');
            }
        } catch (error) {
            console.error('Failed to authenticate:', error);
            setError('An error occurred. Please try again later.');
        } finally {
            setLoading(false);
        }
    };

    return (
        <LoginForm
            username={username}
            setUsername={setUsername}
            password={password}
            setPassword={setPassword}
            loading={loading}
            error={error}
            handleSubmit={handleSubmit}
        />
    );
}
