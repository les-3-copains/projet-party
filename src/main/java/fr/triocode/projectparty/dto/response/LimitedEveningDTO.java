package fr.triocode.projectparty.dto.response;

import fr.triocode.projectparty.data.enums.TypeEvening;
import fr.triocode.projectparty.dto.request.RegisterUserDTO;
import lombok.Data;

import java.util.Date;

@Data
public class LimitedEveningDTO {
    private int id;
    private RegisterUserDTO organizer;
    private String name;
    private String description;
    private int maxGuest;
    private int guestCount;
    private double price;
    private Date startDate;
    private Date endDate;
    private Date createdDate;
    private TypeEvening type;
    private boolean isBringFood;
    private boolean isBringVideoGame;
    private boolean isBringBoardGame;
}
