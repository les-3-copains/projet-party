package fr.triocode.projectparty.dto.response;

import fr.triocode.projectparty.dto.request.RegisterUserDTO;
import lombok.Data;

import java.util.Date;

@Data
public class MessageDTO {
    private int id;
    private String message;
    private Date dateTime;
    private RegisterUserDTO user;
}
