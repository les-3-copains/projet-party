package fr.triocode.projectparty.controller;

import fr.triocode.projectparty.data.enums.Role;
import fr.triocode.projectparty.dto.request.LoginUserDTO;
import fr.triocode.projectparty.dto.request.RegisterUserDTO;
import fr.triocode.projectparty.dto.response.JwtResponse;
import fr.triocode.projectparty.dto.response.UserDTO;
import fr.triocode.projectparty.service.UserService;
import fr.triocode.projectparty.service.security.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<UserDTO> users = userService.getAllUsers();

        List<UserDTO> filteredUsers = users.stream()
                .filter(this::isValid)
                .collect(Collectors.toList());

        return ResponseEntity.ok(filteredUsers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable int id) {
        UserDTO user = userService.getUserById(id);
        if (user != null && isValid(user)) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> createUser(@RequestBody RegisterUserDTO requestUserDTO) {
        UserDTO createdUser = userService.saveUser(requestUserDTO);
        if (createdUser != null) {
            return ResponseEntity.ok(createdUser);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody LoginUserDTO loginUserDTO) {
        if (userService.existsByName(loginUserDTO.getName())) {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginUserDTO.getName(),
                            loginUserDTO.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

            String token = jwtService.createToken(
                    List.of(new SimpleGrantedAuthority(Role.GUEST.name()), new SimpleGrantedAuthority(Role.ORGANIZER.name())),
                    authentication.getName()
            );
            return ResponseEntity.ok(JwtResponse.from(token));
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/modify")
    public ResponseEntity<UserDTO> updateUser(@RequestBody RegisterUserDTO updatedUser) {
        UserDTO user = userService.updateUser(updatedUser);
        if (user != null && isValid(user)) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        if (userService.deleteUser(id)) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private boolean isValid(UserDTO user) {
        return user.getName() != null;
    }
}
