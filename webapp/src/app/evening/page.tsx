'use client'

import {Header} from "@/components/component/header";
import React from "react";
import {Footer} from "@/components/component/footer";
import {EveningForm} from "@/components/component/evening-form";

const Evening = () => {
    return (
        <div className="flex flex-col min-h-screen">
            <Header />
            <EveningForm />
            <Footer/>
        </div>
    );
}
export default Evening;