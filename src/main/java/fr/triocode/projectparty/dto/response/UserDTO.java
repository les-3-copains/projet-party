package fr.triocode.projectparty.dto.response;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserDTO {
    private int id;
    private String city;
    private Date birthday;
    private List<String> interests;
    private String name;
    private List<Integer> ratesGuest;
    private List<Integer> ratesOrganizer;
}
