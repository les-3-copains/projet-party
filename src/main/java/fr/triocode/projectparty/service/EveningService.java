package fr.triocode.projectparty.service;

import fr.triocode.projectparty.data.*;
import fr.triocode.projectparty.data.enums.TypeEvening;
import fr.triocode.projectparty.data.enums.TypeStock;
import fr.triocode.projectparty.dto.request.RequestEveningDTO;
import fr.triocode.projectparty.dto.request.RequestMessageDTO;
import fr.triocode.projectparty.dto.request.RequestStockDTO;
import fr.triocode.projectparty.dto.response.EveningDTO;
import fr.triocode.projectparty.dto.response.LimitedEveningDTO;
import fr.triocode.projectparty.dto.response.MessageDTO;
import fr.triocode.projectparty.dto.response.StockDTO;
import fr.triocode.projectparty.mapper.*;
import fr.triocode.projectparty.repository.*;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EveningService {
    @Autowired
    private EveningRepository eveningRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private EveningMapper eveningMapper;

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private StockMapper stockMapper;

    @Cacheable(value = "evenings", key = "#pageable.pageNumber + '-' + #pageable.pageSize")
    public Page<LimitedEveningDTO> getAllEvenings(Pageable pageable) {
        return eveningRepository.findAll(pageable).map(eveningMapper::toLimitedDto);
    }

    @Cacheable(value = "evenings_search", key = "#pageable.pageNumber + '-' + #pageable.pageSize + '-' + #name + '-' + #city + '-' + #date + '-' + #type")
    public Page<LimitedEveningDTO> getAllSearch(Pageable pageable, String name, String city, Date date, TypeEvening type) {
        return eveningRepository.findAllByNameAndAddress_City_NameAndTypeAndStartDateBeforeAndEndDateAfter(
                name, city, type, date, pageable
        ).map(eveningMapper::toLimitedDto);
    }

    @Cacheable(value = "evenings_allow", key = "#pageable.pageNumber + '-' + #pageable.pageSize")
    public Page<EveningDTO> getAllEveningsAllow(Pageable pageable) {
        return eveningRepository.findAll(pageable).map(eveningMapper::toDto);
    }

    @CachePut(value = "evenings", key = "#result.id")
    public LimitedEveningDTO saveEvening(RequestEveningDTO requestEveningDTO) throws BadRequestException {
        Evening evening = eveningMapper.toRequestEntity(requestEveningDTO);
        if (evening.getType() == TypeEvening.CLASSIC) {
            if (evening.isBringBoardGame() || evening.isBringVideoGame()) {
                throw new BadRequestException("Bad Request : " + evening.getType() + " don't match with isBringVideoGame and isBringBoardGame.");
            }
        } else if (evening.getType() == TypeEvening.BOARD_GAME && evening.isBringVideoGame()) {
            throw new BadRequestException("Bad Request : " + evening.getType() + " don't match with isBringVideoGame.");
        }
        evening.setCreateDate(new Date());

        User organizer = evening.getOrganizer();
        if (organizer != null) {
            organizer = userRepository.findByName(organizer.getName()).orElse(null);
        }
        if (organizer != null) {
            evening.setOrganizer(organizer);
            Address address = evening.getAddress();
            if (address != null) {
                City city = address.getCity();
                if (city != null && !cityRepository.existsByName(city.getName())) {
                    city = cityRepository.save(city);
                    address.setCity(city);
                } else if (city != null) {
                    city = cityRepository.findByNameAndZipCode(city.getName(), city.getZipCode());
                    address.setCity(city);
                }

                if (!addressRepository.existsByNumberAndStreetAndCity(address.getNumber(), address.getStreet(), address.getCity())) {
                    address = addressRepository.save(address);
                    evening.setAddress(address);
                } else {
                    address = addressRepository.findByNumberAndStreetAndCity(address.getNumber(), address.getStreet(), address.getCity());
                    evening.setAddress(address);
                }
            }

            return eveningMapper.toLimitedDto(eveningRepository.save(evening));
        }
        return null;
    }

    @CachePut(value = "evenings", key = "#requestEveningDTO.id")
    public LimitedEveningDTO updateEvening(RequestEveningDTO requestEveningDTO) {
        if (eveningRepository.existsById(requestEveningDTO.getId())) {
            return eveningMapper.toLimitedDto(eveningRepository.save(eveningMapper.toRequestEntity(requestEveningDTO)));
        }
        return null;
    }

    @CacheEvict(value = "evenings", key = "#id")
    public boolean deleteEvening(int id) {
        if (eveningRepository.existsById(id)) {
            eveningRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<MessageDTO> getEveningMessages(int id) {
        if (eveningRepository.existsById(id)) {
            return messageRepository.findAllByEvening_Id(id).stream().map(messageMapper::toDTO).toList();
        }
        return new ArrayList<>();
    }

    public MessageDTO insertMessage(int eveningId, RequestMessageDTO requestMessageDTO) {
        if (eveningId == requestMessageDTO.getEveningId()) {
            Evening evening = eveningRepository.findById(eveningId).orElse(null);
            Message message = messageMapper.toRequestEntity(requestMessageDTO);
            message.setEvening(evening);
            User sender = userRepository.findById(requestMessageDTO.getUserId()).orElse(null);
            message.setUser(sender);
            message.setDateTime(new Date());
            return messageMapper.toDTO(messageRepository.save(message));
        }
        return null;
    }

    public boolean addGuestEvening(int eveningId, int userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null && eveningRepository.existsByIdAndBlacklistedNotContains(eveningId, user)) {
            Evening evening = eveningRepository.findById(eveningId).orElse(null);
            if (evening != null) {
                evening.getGuests().add(user);
                eveningRepository.save(evening);
                return true;
            }
            return false;
        }
        return false;
    }

    public EveningDTO addParticipateEvening(int eveningId, int userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null && eveningRepository.existsByIdAndGuestsContains(eveningId, user)) {
            Evening evening = eveningRepository.findById(eveningId).orElse(null);
            if (evening != null) {
                evening.getParticipants().add(user);
                evening.getGuests().remove(user);
                return eveningMapper.toDto(eveningRepository.save(evening));
            }
            return null;
        }
        return null;
    }

    public boolean addBlacklistEvening(int eveningId, int userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            Evening evening = eveningRepository.findById(eveningId).orElse(null);
            if (evening != null) {
                evening.getBlacklisted().add(user);
                evening.getGuests().remove(user);
                evening.getParticipants().remove(user);
                eveningRepository.save(evening);
                return true;
            }
            return false;
        }
        return false;
    }

    public List<StockDTO> addStock(int idEvening, RequestStockDTO requestStockDTO) throws BadRequestException {
        User user = userRepository.findById(requestStockDTO.getBringerId()).orElse(null);
        if (idEvening == requestStockDTO.getEveningId() && user != null && eveningRepository.existsByIdAndParticipantsContains(idEvening, user)) {
            Evening evening = eveningRepository.findById(idEvening).orElse(null);
            Stock stock = stockMapper.toEntity(requestStockDTO);
            assert evening != null;
            if ((evening.isBringFood() && stock.getType() == TypeStock.FOOD)
                    || (evening.isBringBoardGame() && stock.getType() == TypeStock.BOARD_GAME)
                    || (evening.isBringVideoGame() && stock.getType() == TypeStock.VIDEO_GAME)) {
                stock.setBringer(user);
                stock.setEvening(evening);
                stockRepository.save(stock);
                evening.getStocks().add(stock);
                return evening.getStocks().stream().map(stockMapper::toDTO).toList();
            }
            throw new BadRequestException("Le type : " + stock.getType() + " ne coïncide pas avec le type de soirée : " + evening.getType());
        }
        throw new BadRequestException("Les Id ne correspondent pas");
    }

    @Cacheable(value = "stocks", key = "#idEvening")
    public List<StockDTO> getAllStock(int idEvening) {
        return stockRepository.findAllByEvening_id(idEvening).stream().map(stockMapper::toDTO).toList();
    }
}
