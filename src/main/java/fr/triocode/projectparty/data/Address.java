package fr.triocode.projectparty.data;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;

import java.util.List;

@Entity
@Table(name = "Address", indexes = {
        @Index(name = "idx_address_city_id", columnList = "city_id"),
        @Index(name = "idx_address_number", columnList = "number")
})
@EnableCaching
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = true)
    private int number;

    @Column(nullable = true, length = 100)
    private String street;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    @OneToMany
    private List<Evening> evenings;
}
