package fr.triocode.projectparty.dto.request;

import fr.triocode.projectparty.data.enums.TypeEvening;
import fr.triocode.projectparty.dto.AddressDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
public class RequestEveningDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private RegisterUserDTO organizer;
    private String name;
    private AddressDTO address;
    private String description;
    private int maxGuest;
    private double price;
    private Date startDate;
    private Date endDate;
    private TypeEvening type;
    private boolean isBringFood;
    private boolean isBringVideoGame;
    private boolean isBringBoardGame;
}
