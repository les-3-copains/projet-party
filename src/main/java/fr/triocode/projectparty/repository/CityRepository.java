package fr.triocode.projectparty.repository;

import fr.triocode.projectparty.data.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {
    boolean existsByName(String name);
    City findByNameAndZipCode(String name, int zipCode);
}
