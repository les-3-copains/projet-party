'use client'
import { Input } from "@/components/ui/input";
import { Select, SelectTrigger, SelectValue, SelectContent, SelectGroup, SelectItem } from "@/components/ui/select";
import { Button } from "@/components/ui/button";
import {useEffect, useState} from "react";

interface SearchPartyProps {
  onSearch: (criteria: {
    dateStart: string;
    type: string;
    name: string;
    location: string
  }) => void;
}

export function SearchParty({ onSearch }: SearchPartyProps) {
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [dateStart, setDateStart] = useState("");
  const [types, setTypes] = useState("");
  const [eventTypes, setEventTypes] = useState<string[]>([]);

  useEffect(() => {
    fetch('./data.json')
        .then(response => response.json())
        .then(data => {
          setEventTypes(Array.from(new Set(data.map((event: { type: string; }) => event.type))));
        });
  }, []);

  const handleSearch = (event: { preventDefault: () => void; }) => {
    event.preventDefault();
    onSearch({ name, location, dateStart, type: types });
  };

  return (
      <div className="">
        <div className="">
          <h2 className="text-2xl font-bold mb-4">Rechercher une soirée</h2>
          <form className="grid grid-cols-1 sm:grid-cols-4 gap-2" onSubmit={handleSearch}>
            <div className="col-span-1 sm:col-span-3 flex items-center">
              <Input
                  id="name"
                  type="text"
                  placeholder="Nom de la soirée"
                  className="rounded-r-none"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
              />
              <Input
                  id="location"
                  type="text"
                  placeholder="Lieu de la soirée"
                  className="rounded-l-none rounded-r-none"
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
              />
              <Input
                  id="date"
                  type="date"
                  className="rounded-l-none"
                  value={dateStart}
                  onChange={(e) => setDateStart(e.target.value)}
              />
              <Select onValueChange={setTypes}>
                <SelectTrigger className="w-full">
                  <SelectValue placeholder="Sélectionnez un type" />
                </SelectTrigger>
                <SelectContent>
                  {eventTypes.map((type) => (
                      <SelectGroup key={type}>
                        <SelectItem value={type}>{type}</SelectItem>
                      </SelectGroup>
                  ))}
                </SelectContent>
              </Select>
            </div>
            <div className="col-span-1 flex items-end">
              <Button className="w-full" type="submit">Rechercher</Button>
            </div>
          </form>
        </div>
      </div>
  );
}
