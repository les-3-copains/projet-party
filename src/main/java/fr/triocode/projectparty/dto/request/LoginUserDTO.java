package fr.triocode.projectparty.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class LoginUserDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private String name;
    private String password;
}
