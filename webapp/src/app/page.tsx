'use client'

import { useRouter } from "next/navigation";
import React, {useEffect, useState} from "react";
import { Header } from "@/components/component/header";
import { Footer } from "@/components/component/footer";
import {CardParty} from "@/components/component/card";
import {SearchParty} from "@/components/component/search-party";
import {
    Pagination,
    PaginationContent, PaginationEllipsis,
    PaginationItem,
    PaginationLink, PaginationNext,
    PaginationPrevious
} from "@/components/ui/pagination";
import axios from "@/lib/axios";


interface SearchCriteria {
    name: string;
    location: string;
    dateStart: string;
    type: string;
}

interface Organizer {
    id: number;
    name: string;
    email: string;
}

interface Guest {
    id: number;
    name: string;
    email: string;
}

interface Stock {
    item: string;
    quantity: number;
}

interface Address {
    street: string;
    city: string;
    postalCode: string;
    country: string;
}

interface Evening {
    id: number;
    organizer: Organizer;
    name: string;
    address: Address;
    description: string;
    maxGuest: number;
    guests: Guest[];
    price: number;
    startDate: string;
    endDate: string;
    createdDate: string;
    type: string;
    isBringFood: boolean;
    isBringVideoGame: boolean;
    isBringBoardGame: boolean;
    stocks: Stock[];
}

function isSameDay(date1: Date, date2: Date): boolean {
    return (
        date1.getFullYear() === date2.getFullYear() &&
        date1.getMonth() === date2.getMonth() &&
        date1.getDate() === date2.getDate()
    );
}

export default function Home() {
    const router = useRouter();
    const [events, setEvents] = useState<Evening[]>([]);
    const [initialEvents, setInitialEvents] = useState<Evening[]>([]);
    const [currentPage, setCurrentPage] = useState(1);

    const pageSize = 12;
    const pageCount = Math.ceil(events.length / pageSize);

    useEffect(() => {
        const account = localStorage.getItem('account');
        if (!account) {
            router.push('/login');
        }
    }, [router]);

    /*useEffect(() => {
        axios.get('/api/evening')
            .then(response => {
                return response.data;
            })
            .then(data => {
                setEvents(data);
                setInitialEvents(data);
                }
            )
            .catch(error => console.error(error));
    }, []);*/

    useEffect(() => {
        fetch('./data.json')
            .then(response => response.json())
            .then(data => {
                    setEvents(data);
                    setInitialEvents(data);
                }
            )
            .catch(error => console.error(error));
    }, []);


    const filterEvents = (criteria: SearchCriteria) => {
        return initialEvents.filter((event) => {
            return (
                (!criteria.name || event.name.includes(criteria.name)) &&
                (!criteria.location || event.address.city.includes(criteria.location)) &&
                (!criteria.dateStart || isSameDay(new Date(event.startDate), new Date(criteria.dateStart))) &&
                (!criteria.type || (event.type && event.type.includes(criteria.type)))
            );
        });
    };

    const handlePageChange = (newPage: number) => {
        setCurrentPage(newPage);
    };

    const handleSearch = (criteria: SearchCriteria) => {
        const filteredEvents = filterEvents(criteria);
        setEvents(filteredEvents);
    };

    return (
        <div className="flex flex-col min-h-screen">
            <Header />
            <main className="flex-grow">
                <SearchParty  onSearch={handleSearch}/>
                <div className="my-8 border-gray-300">
                    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                        {Array.isArray(events) && events.map((event, index) => (
                            <CardParty
                                key={index}
                                evening={event}
                            />
                        ))}
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    );
}