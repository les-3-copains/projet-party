package fr.triocode.projectparty.repository;

import fr.triocode.projectparty.data.Evening;
import fr.triocode.projectparty.data.enums.TypeEvening;
import fr.triocode.projectparty.data.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface EveningRepository extends JpaRepository<Evening, Integer> {

    @Query("SELECT e " +
            "FROM Evening e LEFT JOIN e.address a LEFT JOIN a.city c " +
            "WHERE e.name LIKE %:name% AND c.name LIKE %:city% AND e.type = :type AND e.startDate <= :date AND e" +
            ".endDate < :date")
    Page<Evening> findAllByNameAndAddress_City_NameAndTypeAndStartDateBeforeAndEndDateAfter(
            String name, String city, TypeEvening type, Date date, Pageable pageable
    );

    boolean existsByIdAndBlacklistedNotContains(int id, User user);

    boolean existsByIdAndGuestsContains(int id, User user);

    boolean existsByIdAndParticipantsContains(int id, User user);
}
