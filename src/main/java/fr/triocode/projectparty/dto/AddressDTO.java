package fr.triocode.projectparty.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class AddressDTO {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private int id;
    private int number;
    private String street;
    private CityDTO city;
}
