package fr.triocode.projectparty.dto.response;

import fr.triocode.projectparty.data.enums.TypeEvening;
import fr.triocode.projectparty.dto.AddressDTO;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EveningDTO {
    private int id;
    private UserDTO organizer;
    private String name;
    private AddressDTO address;
    private String description;
    private int maxGuest;
    private List<UserDTO> participants;
    private List<UserDTO> guests;
    private List<UserDTO> blacklisted;
    private double price;
    private Date startDate;
    private Date endDate;
    private Date createdDate;
    private TypeEvening type;
    private boolean isBringFood;
    private boolean isBringVideoGame;
    private boolean isBringBoardGame;
    private List<MessageDTO> messages;
    private List<StockDTO> stocks;
}
