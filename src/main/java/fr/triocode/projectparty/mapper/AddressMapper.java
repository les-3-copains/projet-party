package fr.triocode.projectparty.mapper;

import fr.triocode.projectparty.data.Address;
import fr.triocode.projectparty.dto.AddressDTO;
import org.mapstruct.Mapper;

@Mapper(uses = CityMapper.class, componentModel = "spring")
public interface AddressMapper {
    Address toEntity(AddressDTO addressDTO);
    AddressDTO toDTO(Address address);
}
