package fr.triocode.projectparty.data.enums;

public enum TypeStock {
    FOOD,
    BOARD_GAME,
    VIDEO_GAME;
}
