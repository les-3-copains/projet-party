package fr.triocode.projectparty.controller;

import fr.triocode.projectparty.data.enums.TypeEvening;
import fr.triocode.projectparty.dto.request.RequestEveningDTO;
import fr.triocode.projectparty.dto.request.RequestMessageDTO;
import fr.triocode.projectparty.dto.request.RequestStockDTO;
import fr.triocode.projectparty.dto.response.EveningDTO;
import fr.triocode.projectparty.dto.response.LimitedEveningDTO;
import fr.triocode.projectparty.dto.response.MessageDTO;
import fr.triocode.projectparty.dto.response.StockDTO;
import fr.triocode.projectparty.service.EveningService;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/evening")
public class EveningController {
    @Autowired
    private EveningService eveningService;

    @GetMapping
    public ResponseEntity<Page<LimitedEveningDTO>> getAllEvenings(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        PageRequest pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(eveningService.getAllEvenings(pageable));
    }

    @GetMapping("/search")
    public ResponseEntity<Page<LimitedEveningDTO>> getAllEveningsSearch(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) Date date,
            @RequestParam(required = false)TypeEvening typeEvening
            ) {
        PageRequest pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(eveningService.getAllSearch(pageable, name, city, date, typeEvening));
    }

    @GetMapping("/allow")
    public ResponseEntity<Page<EveningDTO>> getAllEveningsAllow(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        PageRequest pageable = PageRequest.of(page, size);
        return ResponseEntity.ok(eveningService.getAllEveningsAllow(pageable));
    }

    @PostMapping("/add")
    public ResponseEntity<LimitedEveningDTO> addEvening(@RequestBody RequestEveningDTO requestEveningDTO) throws BadRequestException {
        return ResponseEntity.ok(eveningService.saveEvening(requestEveningDTO));
    }

    @PutMapping("/modify")
    public ResponseEntity<LimitedEveningDTO> modifyEvening(@RequestBody RequestEveningDTO requestEveningDTO) {
        return ResponseEntity.ok(eveningService.updateEvening(requestEveningDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEvening(@PathVariable int id) {
        if (eveningService.deleteEvening(id)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{id}/messages")
    public ResponseEntity<List<MessageDTO>> getMessages(@PathVariable int id) {
        return ResponseEntity.ok(eveningService.getEveningMessages(id));
    }

    @PostMapping("/{id}/messages")
    public ResponseEntity<MessageDTO> addMessage(@PathVariable int id, @RequestBody RequestMessageDTO requestMessageDTO) {
        return ResponseEntity.ok(eveningService.insertMessage(id, requestMessageDTO));
    }

    @PutMapping("/{eveningId}/participate")
    public ResponseEntity<Void> participate(@PathVariable int eveningId, @RequestParam int userId) {
        if (eveningService.addGuestEvening(eveningId, userId)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{eveningId}/allow")
    public ResponseEntity<EveningDTO> allow(@PathVariable int eveningId, @RequestParam int userId) {
        return ResponseEntity.ok(eveningService.addParticipateEvening(eveningId, userId));
    }

    @PutMapping("/{eveningId}/blacklist")
    public ResponseEntity<EveningDTO> blacklist(@PathVariable int eveningId, @RequestParam int userId) {
        if (eveningService.addBlacklistEvening(eveningId, userId)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{id}/stock")
    public ResponseEntity<List<StockDTO>> getStock(@PathVariable int id) {
        return ResponseEntity.ok(eveningService.getAllStock(id));
    }

    @PostMapping("/{id}/stock")
    public ResponseEntity<List<StockDTO>> addStocks(@PathVariable int id,
                                                    @RequestBody RequestStockDTO requestStockDTO) {
        try {
            return ResponseEntity.ok(eveningService.addStock(id, requestStockDTO));
        } catch (BadRequestException e) {
            throw new RuntimeException(e);
        }
    }

}
