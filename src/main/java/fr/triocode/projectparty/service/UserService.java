package fr.triocode.projectparty.service;

import fr.triocode.projectparty.data.User;
import fr.triocode.projectparty.dto.request.RegisterUserDTO;
import fr.triocode.projectparty.dto.response.UserDTO;
import fr.triocode.projectparty.mapper.UserMapper;
import fr.triocode.projectparty.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Cacheable(value = "users")
    public List<UserDTO> getAllUsers() {
        return userRepository.findAll().stream().map(userMapper::toDto).toList();
    }

    @Cacheable(value = "users", key = "#id")
    public UserDTO getUserById(int id) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            return userMapper.toDto(user);
        }
        return null;
    }

    @CachePut(value = "users", key = "#userDTO.id")
    public UserDTO saveUser(RegisterUserDTO userDTO) {
        if (!userRepository.existsByName(userDTO.getName())) {
            User user = userMapper.toEntity(userDTO);
            user.setPassword(encoder.encode(userDTO.getPassword()));
            return userMapper.toDto(userRepository.save(user));
        }
        return null;
    }

    @CachePut(value = "users", key = "#userDTO.id")
    public UserDTO updateUser(RegisterUserDTO userDTO) {
        if (userRepository.findById(userDTO.getId()).isPresent() && userDTO.getPassword() != null && !userDTO.getPassword().isEmpty()) {
            User user = userMapper.toEntity(userDTO);
            user.setPassword(encoder.encode(userDTO.getPassword()));
            return userMapper.toDto(userRepository.save(user));
        }
        return null;
    }

    @CacheEvict(value = "users", key = "#userId")
    public boolean deleteUser(int userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

    public boolean existsByName(String name) {
        return userRepository.existsByName(name);
    }
}
