package fr.triocode.projectparty.data.enums;

public enum Role {
    ORGANIZER,
    GUEST
}
