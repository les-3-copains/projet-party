package fr.triocode.projectparty.mapper;

import fr.triocode.projectparty.data.Message;
import fr.triocode.projectparty.dto.request.RequestMessageDTO;
import fr.triocode.projectparty.dto.response.MessageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MessageMapper {
    Message toEntity(MessageDTO messageDTO);

    @Mapping(target = "user", ignore = true)
    @Mapping(target = "evening", ignore = true)
    Message toRequestEntity(RequestMessageDTO requestMessageDTO);

    MessageDTO toDTO(Message message);
}
