package fr.triocode.projectparty.repository;

import fr.triocode.projectparty.data.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    List<Message> findAllByEvening_Id(int eveningId);
}
