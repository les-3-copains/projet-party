import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Checkbox } from "@/components/ui/checkbox";
import Link from "next/link";
import { Button } from "@/components/ui/button";
import React from "react";

interface LoginFormProps {
    username: string;
    setUsername: (username: string) => void;
    password: string;
    setPassword: (password: string) => void;
    loading: boolean;
    error: string;
    handleSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
}

export function LoginForm({
          username,
          setUsername,
          password,
          setPassword,
          loading,
          error,
          handleSubmit,
        }: LoginFormProps) {
    return (
        <div className="flex items-center justify-center min-h-screen">
            <div className="w-full max-w-md p-6 space-y-4 bg-card rounded-md shadow-lg">
                <div className="text-center">
                    <h1 className="text-3xl font-bold">Welcome Back</h1>
                    <p className="text-muted-foreground">Sign in to your account</p>
                </div>
                <form className="space-y-4" onSubmit={handleSubmit}>
                    <div>
                        <Label htmlFor="username">Username</Label>
                        <Input
                            id="username"
                            placeholder="Enter your username"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <div>
                        <Label htmlFor="password">Password</Label>
                        <Input
                            id="password"
                            type="password"
                            placeholder="Enter your password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <div className="flex items-center justify-between">
                        <div className="flex items-center space-x-2">
                            <Checkbox id="stayConnected" />
                            <Label htmlFor="stayConnected" className="text-sm">
                                Stay Connected
                            </Label>
                        </div>
                        <Link href="#" className="text-sm text-primary hover:underline" prefetch={false}>
                            Forgot Password?
                        </Link>
                    </div>
                    {error && <p className="text-red-500 text-sm">{error}</p>}
                    <Button type="submit" className="w-full" disabled={loading}>
                        {loading ? 'Loading...' : 'Sign In'}
                    </Button>
                </form>
                <div className="text-center text-sm text-muted-foreground">
                    Don't have an account?{" "}
                    <Link href="#" className="text-primary hover:underline" prefetch={false}>
                        Sign Up
                    </Link>
                </div>
            </div>
        </div>
    );
}
